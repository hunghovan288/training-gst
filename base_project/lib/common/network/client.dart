import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:ketcau_soft/common/const/string_const.dart';
import 'package:ketcau_soft/common/exceptions/connect_exception.dart';
import 'package:ketcau_soft/common/exceptions/server_exception.dart';
import 'package:ketcau_soft/common/exceptions/timeout_exception.dart';
import 'package:ketcau_soft/common/local/app_cache.dart';
import 'package:ketcau_soft/common/local/local_app.dart';
import 'package:ketcau_soft/common/network/app_header.dart';
import 'package:ketcau_soft/common/network/configs.dart';
import 'package:ketcau_soft/common/utils/log_util.dart';

class AppClient {
  AppHeader? header;
  final LocalApp localApp;
  final AppCache appCache;

  AppClient(this.localApp, this.appCache);

  void setHeader(AppHeader header) {
    this.header = header;
  }

  Future<dynamic> get(String fullUrl) async {
    await _checkConnection();
    var url = Uri.parse('$fullUrl');
    Response? response = await http
        .get(url, headers: header?.toJson() ?? {})
        .timeout(Duration(seconds: Configurations.connectTimeout),
            onTimeout: () {
      throw TimeOutException();
    });
    String fullRequets = 'endPoint: $fullUrl\n'
        'Token: ${header?.accessToken}\n'
        'Response: ${response.body}';
    LOG.w('REQUEST_GET: $fullRequets');
    return json.decode(response.body);
  }

  Future<Map> post(
    String endPoint, {
    dynamic body,
    String? contentType,
    bool handleResponse = true,
    bool encodeBody = true,
  }) async {
    await _checkConnection();
    var url = Uri.parse('${Configurations.host}$endPoint');
    Response? response = await http
        .post(url,
            body: body != null ? (encodeBody ? json.encode(body) : body) : null,
            headers: header?.toJson() ?? {'Content-Type': 'application/json'})
        .timeout(Duration(seconds: Configurations.connectTimeout),
            onTimeout: () {
      throw TimeOutException();
    });

    Map<String, dynamic> data = json.decode(response.body);

    String fullRequets = 'endPoint: ${Configurations.host}$endPoint\n'
        'Token: ${header?.accessToken}\n'
        'body: $body\n'
        'Response: ${response.body}';
    LOG.w('REQUEST_POST: $fullRequets');

    return _handleData(data);
  }

  Map<String, dynamic> _handleData(Map<String, dynamic> input) {
    if (!input['status']) {
      throw ServerException(message: input['msg']);
    }
    return input;
  }
  /// check internet
  Future _checkConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {}
    } catch (_) {
      throw ConnectException(message: StringConst.connectError);
    }
  }
}
