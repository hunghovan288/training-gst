import 'package:ketcau_soft/common/const/string_const.dart';
import 'package:ketcau_soft/common/exceptions/app_exception.dart';

class ServerException extends AppException {
  ServerException({
    String? message,
  }) : super(
          message: message ?? StringConst.someThingWentWrong,
        );
}
