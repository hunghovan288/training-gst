import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ketcau_soft/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:ketcau_soft/common/bloc/loading_bloc/loading_event.dart';
import 'package:ketcau_soft/common/bloc/snackbar_bloc/snackbar_bloc.dart';
import 'package:ketcau_soft/common/bloc/snackbar_bloc/snackbar_event.dart';
import 'package:ketcau_soft/common/bloc/snackbar_bloc/snackbar_state.dart';
import 'package:ketcau_soft/common/network/client.dart';
import 'package:ketcau_soft/common/utils/log_util.dart';
import 'package:ketcau_soft/feature/demo/model/search_location_model.dart';
import 'package:ketcau_soft/feature/injector_container.dart';
import 'package:ketcau_soft/feature/widgets/custom_scaffold.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class DemoScreen extends StatefulWidget {
  const DemoScreen({Key? key}) : super(key: key);

  @override
  _DemoScreenState createState() => _DemoScreenState();
}

class _DemoScreenState extends State<DemoScreen> {
  dynamic _data;

  @override
  void initState() {
    _initData();
    super.initState();
  }

  Future<dynamic> getApi(String fullUrl) async {
    var url = Uri.parse('$fullUrl');
    Response? response = await http.get(url).timeout(Duration(seconds: 30));
    LOG.w('response: ${response.body}');
    return json.decode(response.body);
  }

  void _initData() async {
    try {
      List<SearchLocationModel> result = [];
      injector<LoadingBloc>().add(StartLoading());
      final data = await getApi(
          'http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=L7VOAxQp3knXIvfYUr96WBDBjOFq4eXT&q=hanoi');
      setState(() {
        _data = data;
      });
      data.forEach((e) {
        result.add(SearchLocationModel.fromJson(e));
      });
      LOG.d('${result.length}');
    } catch (e) {
      injector<SnackBarBloc>().add(ShowSnackbarEvent(
          type: SnackBarType.error, content: 'Something went wrong!'));
    } finally {
      injector<LoadingBloc>().add(FinishLoading());
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        title: 'Demo Structure',
      ),
      body: Column(
        children: [
          const SizedBox(height: 100),
          _data != null ? Text('$_data') : const SizedBox()
        ],
      ),
    );
  }
}
