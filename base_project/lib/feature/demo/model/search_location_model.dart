class SearchLocationModel {
  int? version;
  String? key;
  String? type;
  int? rank;
  String? localizedName;
  Country? country;
  Country? administrativeArea;

  SearchLocationModel.fromJson(Map<String, dynamic> json) {
    version = json['Version'];
    key = json['Key'];
    type = json['Type'];
    rank = json['Rank'];
    localizedName = json['LocalizedName'];
    country =
        json['Country'] != null ? new Country.fromJson(json['Country']) : null;
    administrativeArea = json['AdministrativeArea'] != null
        ? new Country.fromJson(json['AdministrativeArea'])
        : null;
  }
}

class Country {
  String? iD;
  String? localizedName;

  Country.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    localizedName = json['LocalizedName'];
  }
}
