import 'package:flutter/material.dart';
import 'package:ketcau_soft/common/const/string_const.dart';
import 'package:ketcau_soft/common/utils/screen_utils.dart';
import 'package:ketcau_soft/feature/widgets/custom_button.dart';

class LayoutContainButtonBottom extends StatelessWidget {
  final Function onTap;
  final String? text;
  final double? height;
  final List<BoxShadow>? shadow;
  final TextStyle? textStyle;
  final bool enable;

  const LayoutContainButtonBottom({Key? key,
    required this.onTap,
    this.text,
    this.height,
    this.shadow,
    this.textStyle,
    this.enable = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          boxShadow: shadow ?? StringConst.defaultShadow,
          color: Colors.white),
      height: height ?? GScreenUtil.bottomBarHeight + 56,
      padding: EdgeInsets.only(top: 8.0),
      child: Column(
        children: [
          CustomButton(onTap: enable ? onTap : () {}, text:   text ?? '',)
        ],
      ),
    );
  }
}
