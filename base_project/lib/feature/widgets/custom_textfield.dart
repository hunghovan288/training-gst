import 'package:flutter/material.dart';
import 'package:ketcau_soft/feature/themes/theme_color.dart';
import 'package:ketcau_soft/feature/themes/theme_text.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? hintText;
  final Widget? right;
  final String? label;
  final bool? obscureText;
  final int? maxLine;
  final FormFieldValidator<String>? validator;

  const CustomTextField({
    Key? key,
    this.controller,
    this.hintText,
    this.right,
    this.label,
    this.obscureText,
    this.maxLine,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label ?? hintText ?? '',
          style: AppTextTheme.normalBlack,
        ),
        Stack(
          children: [
            Container(
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(color: AppColors.grey5, width: 1),
                borderRadius: BorderRadius.circular(6),
              ),
              child: TextFormField(
                controller: controller,
                obscureText: obscureText ?? false,
                maxLines: maxLine ?? 1,
                validator:validator,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: hintText,
                    contentPadding: EdgeInsets.only(bottom: 4, left: 16),
                    hintStyle: AppTextTheme.smallGrey.copyWith()),
              ),
            ),
            Positioned(
              child: right ?? const SizedBox(),
              right: 0,
            )
          ],
        ),
      ],
    );
  }
}
