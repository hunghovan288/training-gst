import 'package:get_it/get_it.dart';
import 'package:ketcau_soft/common/bloc/event_bus/event_bus_bloc.dart';
import 'package:ketcau_soft/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:ketcau_soft/common/bloc/snackbar_bloc/snackbar_bloc.dart';
import 'package:ketcau_soft/common/local/app_cache.dart';
import 'package:ketcau_soft/common/local/local_app.dart';
import 'package:ketcau_soft/common/network/client.dart';
import 'package:shared_preferences/shared_preferences.dart';

final injector = GetIt.instance;

Future<void> init() async {
  _initCommon();
  _initBloc();
}

void _initBloc() {
  injector.registerLazySingleton(() => EventBusBloc());
  injector.registerLazySingleton(() => LoadingBloc());
  injector.registerLazySingleton(() => SnackBarBloc());
  injector.registerLazySingleton(() => AppCache());
}

void _initCommon() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  injector.registerLazySingleton(() => sharedPreferences);
  injector.registerLazySingleton(() => AppClient(
        injector(),
        injector(),
      ));
  injector.registerLazySingleton(() => LocalApp(injector()));
}
