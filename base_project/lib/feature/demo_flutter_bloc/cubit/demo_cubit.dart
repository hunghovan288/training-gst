import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ketcau_soft/feature/demo_flutter_bloc/cubit/demo_state.dart';

class DemoCubit extends Cubit<DemoState> {
  DemoCubit() : super(DemoInitState());

  void increaseValue(int value) async {
    emit(DemoAddingState());
    await Future.delayed(Duration(seconds: 2));
    emit(DemoAddedState(++value));
  }
}
