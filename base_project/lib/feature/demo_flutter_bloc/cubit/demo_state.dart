abstract class DemoState {}

class DemoInitState extends DemoState {}

class DemoAddingState extends DemoState {

}
class DemoAddedState extends DemoState{
  final int value;

  DemoAddedState(this.value);
}
