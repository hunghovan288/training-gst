import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ketcau_soft/common/network/client.dart';
import 'package:ketcau_soft/feature/demo_flutter_bloc/cubit/demo_cubit.dart';
import 'package:ketcau_soft/feature/demo_flutter_bloc/cubit/demo_state.dart';

class DemoFlutterBlocScreen extends StatefulWidget {
  const DemoFlutterBlocScreen({Key? key}) : super(key: key);

  @override
  _DemoFlutterBlocContainerState createState() =>
      _DemoFlutterBlocContainerState();
}

class _DemoFlutterBlocContainerState extends State<DemoFlutterBlocScreen> {
  DemoCubit _demoCubit = DemoCubit();
  int _value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            BlocBuilder<DemoCubit,DemoState>(
              bloc: _demoCubit,
              builder: (_, state) {
                if (state is DemoAddingState) {
                  return CircularProgressIndicator();
                }
                if (state is DemoAddedState) {
                  return Text(
                    '${state.value}',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  );
                }
                return Text(
                  'initing',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                );
              },

            ),
            const SizedBox(height: 20),
            InkWell(
              onTap: () {
                _demoCubit.increaseValue(_value);
              },
              child: Container(
                decoration:
                    BoxDecoration(color: Colors.blue, shape: BoxShape.circle),
                padding: EdgeInsets.all(20),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
